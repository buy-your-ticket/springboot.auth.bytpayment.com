#!/bin/bash

IMAGE_NAME=auth:1.0
NB_INSTANCES=1

CONTAINER_BASE_NAME=auth

## construction de l'image
docker image build -t ${IMAGE_NAME} .

for i in `seq 1 ${NB_INSTANCES}`
do
	echo "Je suis là"
	docker stop ${CONTAINER_BASE_NAME}${i}
	docker rm ${CONTAINER_BASE_NAME}${i}

	docker run -p 8080:8080 -d --name ${CONTAINER_BASE_NAME}${i} ${IMAGE_NAME}
done
