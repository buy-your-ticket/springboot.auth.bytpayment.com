package com.bytpayment.auth.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.bytpayment.auth.configurations.Configuration;
import com.bytpayment.auth.enums.Role;
import com.bytpayment.auth.helpers.Helper;
import com.bytpayment.auth.jwt.JWTHandler;
import com.bytpayment.auth.models.QUser;
import com.bytpayment.auth.models.User;
import com.bytpayment.auth.repositories.UserRepository;
import com.bytpayment.auth.request.body.RequestBodyPagination;
import com.bytpayment.auth.request.body.RequestUserLogin;
import com.bytpayment.auth.request.body.RequestUserReadAll;
import com.bytpayment.auth.request.response.ResponseLogin;
import com.bytpayment.auth.request.response.ResponseMessage;
import com.bytpayment.auth.request.response.ResponseMultipleUsers;
import com.bytpayment.auth.request.response.ResponseSingleUser;
import com.querydsl.core.BooleanBuilder;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	
	public ResponseMessage deleteById(String id) {
		Optional<User> user = this.userRepository.findById(id);
		if (!user.isPresent()) { // test si optional est vide
			return new ResponseMessage("Aucun utilisateur trouvé", HttpStatus.NOT_FOUND);
		}
		this.userRepository.deleteById(id);
		return new ResponseMessage("L'utilisateur vient d'être supprimé", HttpStatus.OK);
	}
	
	public ResponseSingleUser insertOne(User user) {
		if (!user.validity()) {
			return new ResponseSingleUser("Paramètres de la requête invalides", null, HttpStatus.BAD_REQUEST);
		}
		
		// Erreur si le login a déjà été utilisé
		// Erreur 409 - CONFLICT
		if (!this.userRepository.findByLogin(user.getLogin()).isEmpty()) {
			return new ResponseSingleUser("Login déjà utilisé", null, HttpStatus.BAD_REQUEST);
		}
		
		// Chiffrement du mot de passe. Si le chiffrement échoue,
		// on ne fait pas l'insertion de l'utilisateur
		String encryptedPassword = Helper.MD5Encryption(user.getPassword());
		if (encryptedPassword == null) {
			return new ResponseSingleUser("Le chiffrement du mot de passe a échoué", null, HttpStatus.valueOf(520));
		}
		user.setPassword(encryptedPassword);
		
		String generatedCode = "";
		do {
			generatedCode = Helper.codeGeneration(5, 20);
		} while(!this.userRepository.findByCode(generatedCode).isEmpty());
		user.setCode(generatedCode);
		
		// Ajout du droit USER si aucun droit n'est spécifié au départ
		if (Helper.arrayListEmpty(user.getRoles())) {
			ArrayList<String> roles = new ArrayList<String>();
			roles.add(Role.USER.toString());
			user.setRoles(roles);
		}
		
		// Forcer le lockRevision à 0 au cas où il serait renseigné différent
		// de 0 dans la requête
		if (user.getLockRevision() != 0) {
			user.setLockRevision(0);
		}
		
		// insertion dans la base de données
		User insertedUser = this.userRepository.insert(user);
		insertedUser.setPassword(null); // ne pas afficher le mot de passe au retour de la requête
		return new ResponseSingleUser("Utilisateur créé", insertedUser, HttpStatus.OK);
	}
	
	public ResponseLogin login(RequestUserLogin requestLogin) {		
		if (!requestLogin.isValid()) {
			return new ResponseLogin(null, "Renseignez le login et le password", null, HttpStatus.BAD_REQUEST);
		}
		
		List<User> users =
			this.userRepository.findByLoginAndPassword(
				requestLogin.getLogin(),
				Helper.MD5Encryption(requestLogin.getPassword())
			);
		
		if (users.isEmpty()) {
			return new ResponseLogin(null, "Identifiant ou mot de passe incorrect", null, HttpStatus.NOT_FOUND);
		}
		
		User user = users.get(0).responsePreparation();
		String jwt = JWTHandler.jwtEncode(user);
		if (Helper.strEmpty(jwt)) {
			return new ResponseLogin(null, "Une erreur s'est produite lors de la génération d'une clé JWT", null, HttpStatus.valueOf(520));
		}
		return new ResponseLogin(jwt, "Connexion réussie", user, HttpStatus.OK);
	}
	
	public ResponseMultipleUsers readAll(RequestUserReadAll request) {
		QUser qUser = new QUser("user");
		BooleanBuilder predicate = new BooleanBuilder();
		if (request.getLogin() != null) {
			predicate.and(qUser.login.startsWithIgnoreCase(request.getLogin()));
		}
		if (request.getLastname() != null) {
			predicate.and(qUser.lastname.startsWithIgnoreCase(request.getLastname()));
		}
		if (request.getFirstname() != null) {
			predicate.and(qUser.firstname.startsWithIgnoreCase(request.getFirstname()));
		}
		if (request.getBirthday() != null) {
			predicate.and(qUser.birthday.eq(request.getBirthday()));
		}
		if (request.getEmail() != null) {
			predicate.and(qUser.email.startsWithIgnoreCase(request.getEmail()));
		}
		
		int limit = Configuration.defaultLimit;
		int offset = 0;
		String sortBy = Configuration.defaultSortBy;
		
		if (request.getPagination() != null) {
			limit = request.getPagination().getLimit() < 1 ? Configuration.defaultLimit : request.getPagination().getLimit();
			offset = request.getPagination().getOffset(); // au lieu de null, cette valeur vaut 0
			sortBy = request.getPagination().getSortBy() == null ? Configuration.defaultSortBy : request.getPagination().getSortBy();
		}
		
		Page<User> result = (Page<User>) this.userRepository.findAll(
			predicate,
			PageRequest.of(offset, limit, Sort.by(sortBy))
		);
		
		// extraction des champs
		ArrayList<String> exfields = request.getExfields();
		for (int i = 0; i < result.getContent().size(); i++) {
			result.getContent().get(i).setPassword(null); // mot de passe forcément à NULL toujours
			if (!Helper.arrayListEmpty(exfields)) {
				if (exfields.contains("id")) {
					result.getContent().get(i).setId(null);
				}
				if (exfields.contains("code")) {
					result.getContent().get(i).setCode(null);
				}
				if (exfields.contains("login")) {
					result.getContent().get(i).setLogin(null);
				}
				if (exfields.contains("lastname")) {
					result.getContent().get(i).setLastname(null);
				}
				if (exfields.contains("firstname")) {
					result.getContent().get(i).setFirstname(null);
				}
				if (exfields.contains("birthday")) {
					result.getContent().get(i).setBirthday(null);
				}
				if (exfields.contains("telephones")) {
					result.getContent().get(i).setTelephones(null);
				}
				if (exfields.contains("roles")) {
					result.getContent().get(i).setRoles(null);
				}
				if (exfields.contains("photo")) {
					result.getContent().get(i).setPhoto(null);
				}
			}
		}
		
		return new ResponseMultipleUsers("Résultat de la recherche", result);
	}
	
	public ResponseSingleUser readByCode(String code) {
		List<User> users = this.userRepository.findByCode(code);
		if (users.isEmpty()) {
			return new ResponseSingleUser("Aucun utilisateur trouvé", null, HttpStatus.NOT_FOUND);
		}
		User user = users.get(0);
		user = user.responsePreparation();
		return new ResponseSingleUser("Un utilisateur trouvé", user, HttpStatus.OK);
	}
	
	public ResponseSingleUser readById(String id) {
		Optional<User> user = this.userRepository.findById(id);
		if (!user.isPresent()) {
			return new ResponseSingleUser("Aucun utilisateur trouvé", null, HttpStatus.NOT_FOUND);
		}
		User u = user.get();
		u = u.responsePreparation();
		
		return new ResponseSingleUser("Un utilisateur trouvé", u, HttpStatus.OK);
	}
	
	public ResponseMultipleUsers readByRole(String role, RequestBodyPagination pagination) {
		int limit = 
			pagination.getLimit() < 1 ? Configuration.defaultLimit : pagination.getLimit();
		int offset = pagination.getOffset(); // au lieu de null, cette valeur vaut 0
		String sortBy = pagination.getSortBy() == null ? Configuration.defaultSortBy : pagination.getSortBy();
		
		// Pour cette requête, on ne renvoie pas les photos, la requête risquerait
		// de prendre beaucoup de temps
		Page<User> result = (Page<User>) this.userRepository.findByRoles(role, PageRequest.of(offset, limit, Sort.by(sortBy)));
		for (int i = 0; i < result.getContent().size(); i++) {
			result.getContent().get(i).setPhoto(null);
			result.getContent().get(i).setPassword(null);
		}
		return new ResponseMultipleUsers(
			"Résultat de la recherche",
			result
		);
	}
	
	public ResponseMultipleUsers readByTelephone(String telephone, RequestBodyPagination pagination) {
		int limit = 
			pagination.getLimit() < 1 ? Configuration.defaultLimit : pagination.getLimit();
		int offset = pagination.getOffset(); // au lieu de null, cette valeur vaut 0
		String sortBy = pagination.getSortBy() == null ? Configuration.defaultSortBy : pagination.getSortBy();
		Page<User> result = (Page<User>) this.userRepository.findByTelephones(telephone, PageRequest.of(offset, limit, Sort.by(sortBy)));
		for (int i = 0; i < result.getContent().size(); i++) {
			result.getContent().get(i).setPhoto(null);
			result.getContent().get(i).setPassword(null);
		}
		return new ResponseMultipleUsers(
			"Résultat de la recherche",
			result
		);
	}
	
	public ResponseLogin update(User user) {
		if (user.getId() == null) {
			return new ResponseLogin(null, "Renseigner l'id de l'utilisateur", null, HttpStatus.BAD_REQUEST);
		}
		Optional<User> lastRecord = this.userRepository.findById(user.getId());
		if (!lastRecord.isPresent()) {
			return new ResponseLogin(null, "Utilisateur non trouvé", null, HttpStatus.NOT_FOUND);
		}
		
		// Contraintes à respecter
		// 1. On ne peut pas changer le code
		// 2. On peut bien changer le login (TODO)
		// 3. Les Roles ne doivent pas être vides
		// 4. On ne peut pas forcer la modification du lockRevision
		// 5. À la fin de la modification, retourner une réponse avec JWT
		
		User userToUpdate = lastRecord.get();
		
		if (user.getPassword() != null) {
			String encryptedPassword = Helper.MD5Encryption(user.getPassword());
			if (encryptedPassword == null) {
				return new ResponseLogin(null, "Erreur de chiffrement du mot de passe", null, HttpStatus.valueOf(520));
			}
			userToUpdate.setPassword(encryptedPassword);
		}		
		if (user.getLastname() != null) {
			userToUpdate.setLastname(user.getLastname());
		}
		if (user.getFirstname() != null) {
			userToUpdate.setFirstname(user.getFirstname());
		}
		if (user.getBirthday() != null) {
			userToUpdate.setBirthday(user.getBirthday());
		}
		if (user.getTelephones() != null) {
			userToUpdate.setTelephones(user.getTelephones());
		}
		if (user.getEmail() != null) {
			userToUpdate.setEmail(user.getEmail());
		}
		if (user.getRoles() != null && !user.getRoles().isEmpty()) {
			userToUpdate.setRoles(user.getRoles());
		}
		if (user.getPhoto() != null) {
			userToUpdate.setPhoto(user.getPhoto());
		}
		
		userToUpdate.setLockRevision(userToUpdate.getLockRevision() + 1);
		
		User savedInstance = this.userRepository.save(userToUpdate);
		
		savedInstance = savedInstance.responsePreparation();
		String jwt = JWTHandler.jwtEncode(savedInstance);
		if (Helper.strEmpty(jwt)) {
			return new ResponseLogin(null, "Une erreur s'est produite lors de la génération d'une clé JWT", null, HttpStatus.valueOf(520));
		}
		return new ResponseLogin(jwt, "Connexion réussie", savedInstance, HttpStatus.OK);
	}
}
