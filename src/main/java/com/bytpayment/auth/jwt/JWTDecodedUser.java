package com.bytpayment.auth.jwt;

import java.util.List;

public class JWTDecodedUser {
	private String id;
	private String code;
	private List<String> roles;
	
	public JWTDecodedUser(String id, String code, List<String> roles) {
		this.id = id;
		this.code = code;
		this.roles = roles;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public List<String> getRoles() {
		return roles;
	}
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
}
