package com.bytpayment.auth.jwt;

public class JWTResult {
	private String message;
	private JWTDecodedUser jwtDecodedUser;
	
	public JWTResult(String message, JWTDecodedUser jwtDecodedUser) {
		this.message = message;
		this.jwtDecodedUser = jwtDecodedUser;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public JWTDecodedUser getJwtDecodedUser() {
		return jwtDecodedUser;
	}

	public void setJwtDecodedUser(JWTDecodedUser jwtDecodedUser) {
		this.jwtDecodedUser = jwtDecodedUser;
	}
}
