package com.bytpayment.auth.jwt;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.bytpayment.auth.configurations.Configuration;
import com.bytpayment.auth.enums.Role;
import com.bytpayment.auth.helpers.Helper;
import com.bytpayment.auth.models.User;

// https://github.com/auth0/java-jwt

public class JWTHandler {
	public static String jwtEncode(User user) {
		Boolean setTokenExp = false;
		
		for (Role role : Configuration.ROLES_WITH_EXP_TOKEN) {
			if (user.getRoles().contains(role.toString())) {
				setTokenExp = true;
				break;
			}
		}
		
		String token = "";
		
		try {
			if (setTokenExp) {
				token = JWT.create()
					.withExpiresAt(getExpiration(new Date())) // on ajoute l'expiration
					.withIssuer(user.getCode())
					.withClaim("id", user.getId())
					.withArrayClaim("roles", user.getRoles().toArray(new String[user.getRoles().size()]))
					.sign(Configuration.jwtAlgorithm);
			} else {
				token = JWT.create()
					.withIssuer(user.getCode())
					.withClaim("id", user.getId())
					.withArrayClaim("roles", user.getRoles().toArray(new String[user.getRoles().size()]))
					.sign(Configuration.jwtAlgorithm);
			}		
		} catch(JWTCreationException exception) {
			return null;
		}
		return token;
	}
	
	// token expiration
	public static Date getExpiration(Date date) {
		// https://www.mkyong.com/java/java-how-to-add-days-to-current-date/
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.MINUTE, Configuration.tokenDuration); 
		
		return c.getTime();
	}
	
	// décoder un JWT
	public static JWTDecodedUser jwtDecode(String token) {
		try {
			Algorithm algorithm = Configuration.jwtAlgorithm;
			JWTVerifier verifier = JWT.require(algorithm).build();
			DecodedJWT jwt = verifier.verify(token);
			String code = jwt.getIssuer();
			String id = jwt.getClaim("id").asString();
			List<String> roles = jwt.getClaim("roles").asList(String.class);
			return new JWTDecodedUser(id, code, roles);
		} catch(JWTVerificationException e) {
			return null;
		}
	}
	
	public static JWTResult getResult(String token) {
		if (token == null) {
			return new JWTResult("Authentification nécessaire pour effectuer cette opération", null);
		}
		String t = token.replaceAll("Bearer\\s*", "");
		JWTDecodedUser jwtDecodedUser = JWTHandler.jwtDecode(t);
		if (jwtDecodedUser == null) {
			return new JWTResult("L'authentification a échoué", null);
		}
		if (Helper.strEmpty(jwtDecodedUser.getCode())) {
			return new JWTResult("L'authentification a échoué, utilisateur inconnu", null);
		}
		return new JWTResult(null, jwtDecodedUser);
	}
}
