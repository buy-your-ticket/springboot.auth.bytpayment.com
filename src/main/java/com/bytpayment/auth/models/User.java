package com.bytpayment.auth.models;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.bytpayment.auth.enums.Role;
import com.bytpayment.auth.helpers.Helper;
import com.querydsl.core.annotations.QueryEntity;

@QueryEntity
@Document(collection = "Users")
public class User {
	@Id
	private String id;
	private String code;
	private String login;
	private String password;
	private String lastname; // nom de famille
	private String firstname; // prénom
	private Date birthday;
	private ArrayList<String> telephones;
	private String email;
	private int lockRevision;
	private ArrayList<String> roles;
	private String photo; // Base64
		
	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public User(String code, String login, String password, String lastname, String firstname, Date birthday,
			ArrayList<String> telephones, String email, ArrayList<String> roles, String photo) {
		this.code = code;
		this.login = login;
		this.password = password;
		this.lastname = lastname;
		this.firstname = firstname;
		this.birthday = birthday;
		this.telephones = telephones;
		this.email = email;
		this.roles = roles;
		this.lockRevision = 0;
		this.photo = photo;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public ArrayList<String> getTelephones() {
		return telephones;
	}

	public void setTelephones(ArrayList<String> telephones) {
		this.telephones = telephones;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getLockRevision() {
		return lockRevision;
	}

	public void setLockRevision(int lockRevision) {
		this.lockRevision = lockRevision;
	}

	public ArrayList<String> getRoles() {
		return roles;
	}

	public void setRoles(ArrayList<String> roles) {
		this.roles = roles;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	/**
	 * Méthode qui permet de tester si un utilisateur est valide
	 * avant son insertion dans la base de données (les attributs à tester proviennent du corps de la requête)
	 * Pour qu'un utilisateur soit valide à la création, il faut qu'il ait au moins :
	 * - nom (lastname)
	 * - prénom (firstname)
	 * - login
	 * - password 
	 * @return boolean
	 */
	public Boolean validity() {
		return (
			!Helper.strEmpty(this.getLogin()) &&
			!Helper.strEmpty(this.getPassword()) &&
			!Helper.strEmpty(this.getLastname()) &&
			!Helper.strEmpty(this.getFirstname())
		);
	}
	
	public User responsePreparation() {
		this.setPassword(null);
		if (this.roles.isEmpty()) {
			ArrayList<String> r = new ArrayList<String>();
			r.add(Role.USER.toString());
			this.setRoles(r);
		}
		return this;
	}
	
	public String toString() {
		String result = "";
		result += "Utilisateur lu : \n";
		result += "id : " + this.id + "\n";
		result += "login : " + this.login + "\n";
		result += "password : " + this.password + "\n";
		result += "lastname : " + this.lastname + "\n";
		result += "firstname : " + this.firstname + "\n";
		return result;
	}
}
