package com.bytpayment.auth.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bytpayment.auth.configurations.Configuration;
import com.bytpayment.auth.enums.Role;
import com.bytpayment.auth.jwt.JWTDecodedUser;
import com.bytpayment.auth.jwt.JWTHandler;
import com.bytpayment.auth.jwt.JWTResult;
import com.bytpayment.auth.models.User;
import com.bytpayment.auth.request.body.RequestBodyPagination;
import com.bytpayment.auth.request.body.RequestUserLogin;
import com.bytpayment.auth.request.body.RequestUserReadAll;
import com.bytpayment.auth.request.response.ResponseLogin;
import com.bytpayment.auth.request.response.ResponseMessage;
import com.bytpayment.auth.request.response.ResponseMultipleUsers;
import com.bytpayment.auth.request.response.ResponseSingleUser;
import com.bytpayment.auth.services.UserService;

@CrossOrigin(origins="*", allowedHeaders="*") // on peut faire origins="http://domain.com"
@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<ResponseMessage> delete(@RequestHeader HttpHeaders headers, @PathVariable("id") String id) {
		String token = headers.getFirst(HttpHeaders.AUTHORIZATION);
		JWTResult jwtResult = JWTHandler.getResult(token);
		JWTDecodedUser jwtDecodedUser = jwtResult.getJwtDecodedUser();
		if (jwtDecodedUser == null) {
			return new ResponseEntity<ResponseMessage>(
				new ResponseMessage(jwtResult.getMessage(), HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		} else if (jwtDecodedUser.getId() == null) {
			return new ResponseEntity<ResponseMessage>(
				new ResponseMessage("Authentification nécessaire pour effectuer cette opération", HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		}
		
		Boolean canDeleteUsers = false;
		for (Role role : Configuration.ROLES_DELETE_USERS) {
			if (jwtDecodedUser.getRoles().contains(role.toString())) {
				canDeleteUsers = true;
				break;
			}
		}
		
		if (canDeleteUsers) {
			ResponseMessage respMessage = this.userService.deleteById(id);
			return new ResponseEntity<ResponseMessage>(respMessage, respMessage.getStatus());
		} else {
			return new ResponseEntity<ResponseMessage>(
				new ResponseMessage("Vous ne disposez pas de droits suffisants pour effectuer cette opération", HttpStatus.FORBIDDEN),
				HttpStatus.FORBIDDEN
			);
		}
	}
	
	@PostMapping("/login")
	public ResponseEntity<ResponseLogin> login(@RequestHeader HttpHeaders headers, @RequestBody RequestUserLogin requestLogin) {
		String token = headers.getFirst(HttpHeaders.AUTHORIZATION);
		JWTResult jwtResult = JWTHandler.getResult(token);
		JWTDecodedUser jwtDecodedUser = jwtResult.getJwtDecodedUser();
		if (jwtDecodedUser == null) {
			return new ResponseEntity<ResponseLogin>(
				new ResponseLogin(null, jwtResult.getMessage(), null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		} else if (jwtDecodedUser.getId() != null) {
			return new ResponseEntity<ResponseLogin>(
				new ResponseLogin(
					null,
					"Un utilisateur connecté n'a pas accès à cette requête",
					null,
					HttpStatus.FORBIDDEN
				),
				HttpStatus.FORBIDDEN
			);
		}
		ResponseLogin respLogin = this.userService.login(requestLogin);
		return new ResponseEntity<ResponseLogin>(
			respLogin,
			respLogin.getStatus()
		);
	}
	
	@GetMapping("/read/{id}")
	public ResponseEntity<ResponseSingleUser> read(@RequestHeader HttpHeaders headers, @PathVariable("id") String id) {
		ResponseEntity<ResponseSingleUser> checkAuthorization = this.rsuCheckAuthorizationRequired(headers);
		if (checkAuthorization != null) {
			return checkAuthorization;
		}
		
		ResponseSingleUser rsu = this.userService.readById(id);
		return new ResponseEntity<ResponseSingleUser>(
			rsu,
			rsu.getStatus()
		);
	}
	
	@PostMapping("/readAll")
	public ResponseEntity<ResponseMultipleUsers> readAll(@RequestHeader HttpHeaders headers, @RequestBody RequestUserReadAll request) {
		ResponseEntity<ResponseMultipleUsers> checkAuthorization = this.rmuCheckAuthorization(headers);
		if (checkAuthorization != null) {
			return checkAuthorization;
		}
		
		ResponseMultipleUsers rmu = this.userService.readAll(request);
		return new ResponseEntity<ResponseMultipleUsers>(rmu, HttpStatus.OK);
	}
	
	@GetMapping("/readByCode/{code}")
	public ResponseEntity<ResponseSingleUser> readByCode(@RequestHeader HttpHeaders headers, @PathVariable String code) {
		ResponseEntity<ResponseSingleUser> checkAuthorization = this.rsuCheckAuthorizationRequired(headers);
		if (checkAuthorization != null) {
			return checkAuthorization;
		}
		
		ResponseSingleUser rsu = this.userService.readByCode(code);
		return new ResponseEntity<ResponseSingleUser>(rsu, rsu.getStatus());
	}
	
	@PostMapping("/readByRole/{role}")
	public ResponseEntity<ResponseMultipleUsers> readByRole(
		@RequestHeader HttpHeaders headers,
		@PathVariable("role") String role,
		@RequestBody RequestBodyPagination pagination
	) {
		ResponseEntity<ResponseMultipleUsers> checkAuthorization = this.rmuCheckAuthorization(headers);
		if (checkAuthorization != null) {
			return checkAuthorization;
		}
		ResponseMultipleUsers rmu = this.userService.readByRole(role, pagination);
		return new ResponseEntity<ResponseMultipleUsers>(rmu, HttpStatus.OK);
	}
	
	@PostMapping("/readByTelephone/{telephone}")
	public ResponseEntity<ResponseMultipleUsers> readByTelephone(
		@RequestHeader HttpHeaders headers,
		@PathVariable("telephone") String telephone,
		@RequestBody RequestBodyPagination pagination
	) {
		ResponseEntity<ResponseMultipleUsers> checkAuthorization = this.rmuCheckAuthorization(headers);
		if (checkAuthorization != null) {
			return checkAuthorization;
		}
		
		ResponseMultipleUsers rmu = this.userService.readByTelephone(telephone, pagination);
		return new ResponseEntity<ResponseMultipleUsers>(rmu, HttpStatus.OK);
	}
	
	@PutMapping("/register")
	public ResponseEntity<ResponseSingleUser> registrer(@RequestHeader HttpHeaders headers, @RequestBody User user) {
		ResponseEntity<ResponseSingleUser> checkNoAthorization = this.rsuCheckNoAuthorizationRequired(headers);
		if (checkNoAthorization != null) {
			return checkNoAthorization;
		}
		ResponseSingleUser rsu = this.userService.insertOne(user);
		return new ResponseEntity<ResponseSingleUser>(
			rsu,
			rsu.getStatus()
		);
	}
		
	@PostMapping("/update")
	public ResponseEntity<ResponseLogin> update(@RequestHeader HttpHeaders headers, @RequestBody User user) {
		String token = headers.getFirst(HttpHeaders.AUTHORIZATION);
		JWTResult jwtResult = JWTHandler.getResult(token);
		JWTDecodedUser jwtDecodedUser = jwtResult.getJwtDecodedUser();
		if (jwtDecodedUser == null) {
			return new ResponseEntity<ResponseLogin>(
				new ResponseLogin(null, jwtResult.getMessage(), null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		} else if (jwtDecodedUser.getId() == null) {
			return new ResponseEntity<ResponseLogin>(
				new ResponseLogin(null, "Authentification nécessaire pour effectuer cette opération", null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		}
		
		ResponseLogin respLogin = this.userService.update(user);
		return new ResponseEntity<ResponseLogin>(respLogin, respLogin.getStatus());
	}
	
	// rmu = ResponseMultipleUsers
	private ResponseEntity<ResponseMultipleUsers> rmuCheckAuthorization(HttpHeaders headers) {
		String token = headers.getFirst(HttpHeaders.AUTHORIZATION);
		JWTResult jwtResult = JWTHandler.getResult(token);
		JWTDecodedUser jwtDecodedUser = jwtResult.getJwtDecodedUser();
		if (jwtDecodedUser == null) {
			return new ResponseEntity<ResponseMultipleUsers>(
				new ResponseMultipleUsers(jwtResult.getMessage(), null),
				HttpStatus.UNAUTHORIZED
			);
		} else if (jwtDecodedUser.getId() == null) {
			return new ResponseEntity<ResponseMultipleUsers>(
				new ResponseMultipleUsers("Authentification nécessaire pour effectuer cette opération",null),
				HttpStatus.UNAUTHORIZED
			);
		}
		return null;
	}
	
	// Vérification de l'authentification d'un utilisateur
	// rsu = ResponseSingleUser
	private ResponseEntity<ResponseSingleUser> rsuCheckAuthorizationRequired(HttpHeaders headers) {
		String token = headers.getFirst(HttpHeaders.AUTHORIZATION);
		JWTResult jwtResult = JWTHandler.getResult(token);
		JWTDecodedUser jwtDecodedUser = jwtResult.getJwtDecodedUser();
		if (jwtDecodedUser == null) {
			return new ResponseEntity<ResponseSingleUser>(
				new ResponseSingleUser(jwtResult.getMessage(), null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		} else if (jwtDecodedUser.getId() == null) {
			return new ResponseEntity<ResponseSingleUser>(
				new ResponseSingleUser(
					"Authentification nécessaire pour effectuer cette opération",
					null,
					HttpStatus.UNAUTHORIZED
				),
				HttpStatus.UNAUTHORIZED
			);
		}
		return null;
	}
	
	// Verification de l'utilisation exclusive du token JWT static
	// rsu = ResponseSingleUser
	private ResponseEntity<ResponseSingleUser> rsuCheckNoAuthorizationRequired(HttpHeaders headers) {
		String token = headers.getFirst(HttpHeaders.AUTHORIZATION);
		JWTResult jwtResult = JWTHandler.getResult(token);
		JWTDecodedUser jwtDecodedUser = jwtResult.getJwtDecodedUser();
		if (jwtDecodedUser == null) {
			return new ResponseEntity<ResponseSingleUser>(
				new ResponseSingleUser(jwtResult.getMessage(), null, HttpStatus.UNAUTHORIZED),
				HttpStatus.UNAUTHORIZED
			);
		} else if (jwtDecodedUser.getId() != null) {
			return new ResponseEntity<ResponseSingleUser>(
				new ResponseSingleUser(
					"Un utilisateur connecté n'a pas accès à cette requête",
					null,
					HttpStatus.FORBIDDEN
				),
				HttpStatus.FORBIDDEN
			);
		}
		return null;
	}
	
}
