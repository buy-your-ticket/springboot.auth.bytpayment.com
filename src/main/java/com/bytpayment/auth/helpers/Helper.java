package com.bytpayment.auth.helpers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Random;


public class Helper {	
	public static Boolean strEmpty(String string) {
		return (string == null || string.trim().isEmpty());
	}
	
	public static Boolean arrayListEmpty(ArrayList<String> array) {
		return (array == null || array.isEmpty());
	}
	
	// https://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
	public static String MD5Encryption(String string) {
		String md5 = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(string.getBytes());
			byte[] bytes = md.digest();
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			md5 = sb.toString();
			return md5;
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}
	
	public static String codeGeneration(int minSize, int maxSize) {
		int min, max;
		if (minSize == 0 || minSize >= maxSize ) {
			min = 5;
			max = 20;
		} else {
			min = minSize;
			max = maxSize;
		}
		Random random = new Random();
		int size = random.nextInt((max - min) + 1) + min;
		String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		String code = "";
		for (int i = 0; i < size - 1; i++) {
			code += chars.charAt(random.nextInt(chars.length()));
		}
		return code;
	}
}
