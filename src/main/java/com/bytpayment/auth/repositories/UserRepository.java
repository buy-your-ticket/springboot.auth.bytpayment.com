package com.bytpayment.auth.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.bytpayment.auth.models.User;

public interface UserRepository extends MongoRepository<User, String>, QuerydslPredicateExecutor<User>{
	public List<User> findByLogin(String login);
	
	public List<User> findByCode(String code);
	
	public Optional<User> findById(String id);
	
	@Query(value = "{'login': ?0, 'password': ?1}")
	public List<User> findByLoginAndPassword(String login, String password);
	
	public Page<User> findByTelephones(String telephone, Pageable pageable);
	
	public Page<User> findByRoles(String role, Pageable pageable);
	
	public void deleteById(String id);
}
