package com.bytpayment.auth.configurations;

import java.util.ArrayList;
import java.util.Arrays;

import com.auth0.jwt.algorithms.Algorithm;
import com.bytpayment.auth.enums.Role;

public class Configuration {
	// https://github.com/auth0/java-jwt
	// BYTPAYMENT_KNOWN_CLIENT : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlcyI6bnVsbCwiaXNzIjoiQllUUEFZTUVOVF9LTk9XTl9DTElFTlQiLCJpZCI6bnVsbH0.gBhQArcs6WHQfTrqxBkyw7-ue7WU2a1DHyjdwIND_nU
	public static final String secret = "somesecretmessage";
	public static final Algorithm jwtAlgorithm = Algorithm.HMAC256(secret);
	public static final int tokenDuration = 120; // 120 minutes
	
	// Requête readAll + Pagination
	public static final int defaultLimit = 50;
	public static final String defaultSortBy = "lastname";
	
	// Roles pour lequels on configure une expiration de Token
	public static final ArrayList<Role> ROLES_WITH_EXP_TOKEN =
		new ArrayList<Role>(
			Arrays.asList(Role.ADMIN, Role.SELLER, Role.TECH)
		);
	
	// Utilisateurs autorisés à supprimer d'autres comptes
	public static final ArrayList<Role> ROLES_DELETE_USERS =
		new ArrayList<Role>(Arrays.asList(Role.ADMIN));
}
