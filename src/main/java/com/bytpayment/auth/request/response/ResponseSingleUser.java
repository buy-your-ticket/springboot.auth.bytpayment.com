package com.bytpayment.auth.request.response;

import org.springframework.http.HttpStatus;

import com.bytpayment.auth.models.User;

public class ResponseSingleUser {
	private String message;
	private User user;
	private HttpStatus status;
	
	public ResponseSingleUser(String message, User user, HttpStatus status) {
		this.message = message;
		this.user = user;
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
}
