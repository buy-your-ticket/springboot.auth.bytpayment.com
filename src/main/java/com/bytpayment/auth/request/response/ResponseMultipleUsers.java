package com.bytpayment.auth.request.response;

import org.springframework.data.domain.Page;

import com.bytpayment.auth.models.User;

public class ResponseMultipleUsers {
	private String message;
	private Page<User> results;
	
	public ResponseMultipleUsers(String message, Page<User> results) {
		this.message = message;
		this.results = results;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Page<User> getResults() {
		return results;
	}

	public void setResults(Page<User> results) {
		this.results = results;
	}	
}
