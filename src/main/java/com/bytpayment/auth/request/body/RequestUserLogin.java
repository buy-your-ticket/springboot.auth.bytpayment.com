package com.bytpayment.auth.request.body;

import com.bytpayment.auth.helpers.Helper;

public class RequestUserLogin {
	private String login;
	private String password;
	
	public String getLogin() {
		return login;
	}
	public String getPassword() {
		return password;
	}
	
	public Boolean isValid() {
		return(
			!Helper.strEmpty(this.login) &&
			!Helper.strEmpty(this.password)
		);
	}	
}
