package com.bytpayment.auth.request.body;

public class RequestBodyPagination {
	private int limit;
	private int offset;
	private String sortBy;
	
	public RequestBodyPagination(int limit, int offset, String sortBy) {
		this.limit = limit;
		this.offset = offset;
		this.sortBy = sortBy;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}
	
	
}
