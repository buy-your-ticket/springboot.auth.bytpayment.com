package com.bytpayment.auth.request.body;

import java.util.ArrayList;
import java.util.Date;

public class RequestUserReadAll {
	private String login;
	private String lastname;
	private String firstname;
	private String email;
	private Date birthday;
	private ArrayList<String> exfields; // les champs à ne pas retourner
	private RequestBodyPagination pagination;
	//private int limit;
	//private int offset;
	//private String sortBy;
	
	public RequestUserReadAll(String login, String lastname, String firstname, String email, Date birthday, ArrayList<String> exfields, RequestBodyPagination pagination/*int limit,int offset, String sortBy*/ ) {
		this.login = login;
		this.lastname = lastname;
		this.firstname = firstname;
		this.email = email;
		this.birthday = birthday;
		/*this.limit = limit;
		this.offset = offset;
		this.sortBy = sortBy;*/
		this.exfields = exfields;
		this.pagination = pagination;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public ArrayList<String> getExfields() {
		return exfields;
	}

	public void setExields(ArrayList<String> exfields) {
		this.exfields = exfields;
	}

	public RequestBodyPagination getPagination() {
		return pagination;
	}

	public void setPagination(RequestBodyPagination pagination) {
		this.pagination = pagination;
	}

	/*public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public String getSortBy() {
		return sortBy;
	}

	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}*/
	
}
